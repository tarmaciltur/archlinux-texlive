# ArchLinux image with Texlive

FROM archlinux/base:latest
MAINTAINER tarmaciltur

# Locale
RUN echo 'es_AR.UTF-8 UTF-8' >> /etc/locale.gen && locale-gen
ENV LANG es_AR.UTF-8
ENV LANGUAGE es_AR:es
ENV LC_ALL es_AR.UTF-8

# Update and install needed packages
RUN pacman -Syu --noconfirm --noprogressbar base \ 
                                            base-devel \
                                            texlive-most \
                                            texlive-lang \
                                            biber \
                                            ghostscript
RUN pacman -Syu --noconfirm --noprogressbar python-pip
RUN pip install anybadge 
 
RUN pacman -Syu --noconfirm --noprogressbar git

# Clean cache
RUN pacman -Scc --noconfirm
